import boto3
import paramiko
import os, time

default_zone = 'eu-west-1'

if os.getenv('AWS_DEFAULT_REGION') is None:
    os.environ['AWS_DEFAULT_REGION'] = default_zone

uniq_tag = 'Pavlo/Talaver'

ec2_resource = boto3.resource('ec2')
ec2_client = boto3.client('ec2')

ec2_instance_resource = None
security_group_resource = None


def task_one():
    global ec2_instance_resource

    key_pair_exist = ec2_client.describe_key_pairs(
        Filters=[
            {
                'Name': 'key-name',
                'Values': [
                    uniq_tag,
                ]
            },
        ],
    ).get('KeyPairs', [])

    if key_pair_exist:
        print('Key Pair available')
        for key_pair in key_pair_exist:
            print(key_pair.get('KeyFingerprint'), '\n')
    else:
        print('Creating Key Pair')
        new_key_pair = ec2_client.create_key_pair(
            KeyName=uniq_tag,
        )
        print(new_key_pair.get('KeyFingerprint'), '\n')

        with open('pk.pem', 'w+') as pk_file:
            text = new_key_pair.get('KeyMaterial')
            pk_file.writelines(text)

    instances = ec2_resource.instances.filter(
        Filters=[{'Name': 'tag:Name', 'Values': [uniq_tag]},
                 {'Name': 'instance-state-name',
                  'Values': ['pending', 'running', 'shutting-down', 'stopping', 'stopped']}]
    )

    if sum(1 for instance in instances):
        print('EC2 instance available')
        for instance in instances:
            print(instance.id)
            ec2_instance_resource = instance
        else:
            print('')
    else:
        print('Creating EC2 instance')

        new_instance = ec2_resource.create_instances(ImageId='ami-03ef731cc103c9f09', MinCount=1, MaxCount=1,
                                                     InstanceType='t2.micro',
                                                     KeyName=uniq_tag,
                                                     TagSpecifications=[
                                                         {
                                                             'ResourceType': 'instance',
                                                             'Tags': [
                                                                 {
                                                                     'Key': 'Name',
                                                                     'Value': uniq_tag
                                                                 },
                                                             ]
                                                         },
                                                         {
                                                             'ResourceType': 'volume',
                                                             'Tags': [
                                                                 {
                                                                     'Key': 'Name',
                                                                     'Value': uniq_tag
                                                                 },
                                                             ]
                                                         },
                                                     ],
                                                     )
        for instance in new_instance:
            ec2_instance_resource = instance
        print(ec2_instance_resource.id, '\n')

        ec2_instance_resource.wait_until_exists()


def task_two():
    global security_group_resource

    security_group_iterator = ec2_resource.security_groups.filter(
        GroupNames=[
            uniq_tag,
        ],
    )

    try:
        sum(1 for security_group in security_group_iterator)
    except:
        print('Creating security group')

        security_group_resource = ec2_resource.create_security_group(
            Description='Allow inbound SSH and HTTP connections',
            GroupName=uniq_tag,
        )

        security_group_resource.create_tags(
            Tags=[
                {
                    'Key': 'Name',
                    'Value': uniq_tag
                },
            ]
        )

        print(security_group_resource.id)

        security_group_resource.authorize_ingress(
            IpPermissions=[
                {
                    'FromPort': 22,
                    'IpProtocol': 'tcp',
                    'IpRanges': [
                        {
                            'CidrIp': '0.0.0.0/0',
                            'Description': 'SSH from any IPv4'
                        },
                    ],
                    'Ipv6Ranges': [
                        {
                            'CidrIpv6': '::/0',
                            'Description': 'SSH from any IPv6'
                        },
                    ],
                    'ToPort': 22,
                },
                {
                    'FromPort': 80,
                    'IpProtocol': 'tcp',
                    'IpRanges': [
                        {
                            'CidrIp': '0.0.0.0/0',
                            'Description': 'HTTP from any IPv4'
                        },
                    ],
                    'Ipv6Ranges': [
                        {
                            'CidrIpv6': '::/0',
                            'Description': 'HTTP from any IPv6'
                        },
                    ],
                    'ToPort': 80,
                },
            ],
        )
    else:
        print('Security Group available')
        for security_group in security_group_iterator:
            security_group_resource = security_group
            print(security_group.id)

    if not security_group_resource.id in ec2_instance_resource.security_groups[0].values():
        print('Attaching Security Group\n')
        ec2_instance_resource.modify_attribute(
            Groups=[
                security_group_resource.id,
            ],
        )
    else:
        print('Security Group attached\n')


def task_three():
    volume_exist = ec2_client.describe_volumes(
        Filters=[
            {
                'Name': 'tag:Name',
                'Values': [
                    uniq_tag,
                ],
            },
            {
                'Name': 'size',
                'Values': [
                    '1',
                ],
            },
            {
                'Name': 'volume-type',
                'Values': [
                    'standard',
                ],
            },
        ],
    ).get('Volumes', [])

    if volume_exist:
        volume_resource = ec2_resource.Volume(volume_exist[0].get('VolumeId'))
        print('EBS volume available')
    else:
        print('Creating EBS volume')
        volume_resource = ec2_resource.create_volume(
            AvailabilityZone=ec2_instance_resource.placement.get('AvailabilityZone'),
            Encrypted=False,
            Size=1,
            VolumeType='standard',
            TagSpecifications=[
                {
                    'ResourceType': 'volume',
                    'Tags': [
                        {
                            'Key': 'Name',
                            'Value': uniq_tag
                        },
                    ]
                },
            ]
        )

        waiter = ec2_client.get_waiter('volume_available')

        waiter.wait(
            VolumeIds=[
                volume_resource.id,
            ],
            WaiterConfig={
                'Delay': 10,
                'MaxAttempts': 100
            }
        )

    print(volume_resource.id)

    ec2_instance_resource.wait_until_running()

    if volume_resource.state != 'in-use':
        print('Attaching volume\n')

        volume_resource.attach_to_instance(
            Device='/dev/sdf',
            InstanceId=ec2_instance_resource.id,
        )

        waiter = ec2_client.get_waiter('volume_in_use')

        waiter.wait(
            VolumeIds=[
                volume_resource.id,
            ],
            WaiterConfig={
                'Delay': 10,
                'MaxAttempts': 10
            }
        )

    else:
        print('Volume attached to instance\n')


def task_four():
    print('Connect to the instance via SSH')

    ec2_instance_resource.wait_until_running()

    instance_ip = str(ec2_instance_resource.public_ip_address)
    key = paramiko.RSAKey.from_private_key_file("pk.pem")
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    for i in range(1, 20):
        print('Connection attempt:', i)
        try:
            client.connect(hostname=instance_ip, username="ubuntu", pkey=key, timeout=5)
            break
        except Exception as e:
            print('Error:', e, '\n')
            time.sleep(5)
        finally:
            client.close()

    client.connect(hostname=instance_ip, username="ubuntu", pkey=key, timeout=30)

    stdin, stdout, stderr = client.exec_command('sudo file -s /dev/xvdf')
    if ': data' in str(stdout.read()):
        print('Formatting and mounting')
        for command in 'sudo mkfs -t xfs -f /dev/xvdf', 'sudo mkdir /data', 'sudo mount /dev/xvdf /data':
            stdin, stdout, stderr = client.exec_command(command)
            print(str(stdout.read()))
    else:
        print('Volume mounted to /data')

    client.close()


def main():
    task_one()
    task_two()
    task_three()
    task_four()


if __name__ == "__main__":
    main()
